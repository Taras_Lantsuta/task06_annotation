package com.emap.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.lang.reflect.InvocationTargetException;
import static com.emap.model.AppProperties.prop;
import com.emap.model.Product;
import com.emap.model.ClassInfo;
import com.emap.controller.ProductReflection;

public class Main {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        Product product = new Product(prop("product.name")
                , prop("product.partNumber"), Integer.parseInt(prop("product.price")));
        ProductReflection productReflection = new ProductReflection(product);
        try {
            String concat = productReflection.concat(prop("product.name")
                    , prop("product.partNumber"), Integer.parseInt(prop("product.price")));
            logger.info(concat);
            double priceInUah = productReflection.priceInUah(Integer.parseInt(prop("product.price"))
                    , Double.parseDouble(prop("product.course")));
            logger.info(priceInUah);
            String sum = productReflection.sum(prop("product.name"), Integer.parseInt(prop("product.price"))
                    , Integer.parseInt(prop("product.price")), Integer.parseInt(prop("product.price")));
            logger.info(sum);
            String concatAll = productReflection.concatAll(prop("product.name"), prop("product.partNumber")
                    , prop("product.partNumber"), prop("product.partNumber"));
            logger.info(concatAll);
            productReflection.setPartNumberField(prop("product.name"));
            String partNumberField = productReflection.getPartNumberField();
            logger.info(partNumberField);
            String name = productReflection.getName();
            logger.info(name);
            productReflection.printAnnotationFields();
            ClassInfo.showInfo(product);
        } catch (NoSuchMethodException | InvocationTargetException
                | IllegalAccessException | NoSuchFieldException e) {
            logger.error(e);
        }
    }
}