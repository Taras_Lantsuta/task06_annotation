package com.emap.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ClassInfo {

    private static final Logger logger = LogManager.getLogger();

    public static void showInfo(Object object) {
        Class unknown = object.getClass();
        Field[] declaredFields = unknown.getDeclaredFields();
        Constructor[] declaredConstructors = unknown.getDeclaredConstructors();
        Method[] declaredMethods = unknown.getDeclaredMethods();
        Class[] declaredClasses = unknown.getDeclaredClasses();
        Annotation[] declaredAnnotations = unknown.getDeclaredAnnotations();
        logger.info(unknown.getName());
        for (Field declaredField : declaredFields) {
            logger.info(declaredField.getType() + " " + declaredField.getName());
        }
        for (Constructor declaredConstructor : declaredConstructors) {
            logger.info(declaredConstructor.getModifiers() + " " + declaredConstructor.getName());
        }
        for (Method declaredMethod : declaredMethods) {
            logger.info(declaredMethod.getReturnType() + " " + declaredMethod.getName());
        }
        for (Class declaredClass : declaredClasses) {
            logger.info(declaredClass.getName());
        }
        for (Annotation declaredAnnotation : declaredAnnotations) {
            logger.info(declaredAnnotation.annotationType().getDeclaredFields());
        }
    }
}
