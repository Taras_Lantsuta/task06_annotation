package com.emap.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class AppProperties {

    private static final Logger logger = LogManager.getLogger();
    private static final String PATH;
    private static final Properties APP_PROPERTIES;

    static {
        PATH = Thread.currentThread().getContextClassLoader().getResource("app.properties").getPath();
        APP_PROPERTIES = new Properties();
        try (FileInputStream fileInputStream = new FileInputStream(PATH)){
            APP_PROPERTIES.load(fileInputStream);
        } catch (IOException e) {
            logger.error(e);
        }
    }

    public static String prop(String name) {
        return APP_PROPERTIES.getProperty(name);
    }
}
