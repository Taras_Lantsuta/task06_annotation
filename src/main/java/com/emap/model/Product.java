package com.emap.model;

public class Product {
    @Description(name = "HP", partNumber = "R3000")
    private String name;
    private String partNumber;
    @Description(price = 3000)
    private int price;

    public Product(String name, String partNumber, int price) {
        this.name = name;
        this.partNumber = partNumber;
        this.price = price;
    }

    public String concat(String name, String partNumber, int price) {
        return name.concat(partNumber).concat(String.valueOf(price));
    }

    public String concatAll(String name, String... strings) {
        StringBuilder stringBuilder = new StringBuilder(name);
        for (String string : strings) {
            stringBuilder.append(string);
        }
        return stringBuilder.toString();
    }

    public String sum(String name, int... values) {
        int sum = 0;
        for (int value : values) {
            sum += value;
        }
        return name + " " + sum;
    }

    public double priceInUah(int price, double course) { return price * course; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getPartNumber() { return partNumber; }

    public void setPartNumber(String partNumber) { this.partNumber = partNumber; }

    public int getPrice() { return price; }

    public void setPrice(int price) { this.price = price; }
}


