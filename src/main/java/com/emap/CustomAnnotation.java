package com.emap;

import java.lang.annotation.*;

@Documented
@Target(ElementType.METHOD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)

public @interface CustomAnnotation {
    int id() default 0;
    String name();
    String surname();
    String someInfo() default "Good Man";
}
