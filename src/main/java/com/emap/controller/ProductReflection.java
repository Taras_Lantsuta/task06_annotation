package com.emap.controller;

import com.emap.model.Description;
import com.emap.model.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ProductReflection {

    private static final Logger logger = LogManager.getLogger();
    private Product product;
    private Class clazz;

    public ProductReflection(Product product) {
        this.product = product;
        clazz = product.getClass();
    }

    public String getName() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        Method getName = clazz.getDeclaredMethod("getName");
        return (String) getName.invoke(product);
    }

    public String concat(String name, String partNumber, int price) throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        Method concat = clazz.getDeclaredMethod("concat", String.class, String.class, int.class);
        return (String) concat.invoke(product, name, partNumber, price);
    }

    public String concatAll(String name, String... strings) throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        Method concatAll = clazz.getDeclaredMethod("concatAll", String.class, String[].class);
        return (String) concatAll.invoke(product, name, strings);
    }

    public double priceInUah(int price, double course) throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        Method priceInUah = clazz.getDeclaredMethod("priceInUah", int.class, double.class);
        return (double) priceInUah.invoke(product, price, course);
    }

    public String sum(String name, int... values) throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        Method sum = clazz.getDeclaredMethod("sum", String.class, int[].class);
        return (String) sum.invoke(product, name, values);
    }

    public void setPartNumberField(String value) throws NoSuchFieldException, IllegalAccessException {
        Field partNumber = clazz.getDeclaredField("partNumber");
        partNumber.setAccessible(true);
        partNumber.set(product, value);
    }

    public String getPartNumberField() throws NoSuchFieldException, IllegalAccessException {
        Field partNumber = clazz.getDeclaredField("partNumber");
        partNumber.setAccessible(true);
        return (String) partNumber.get(product);
    }

    public void printAnnotationFields() {
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            if (declaredField.isAnnotationPresent(Description.class)) {
                declaredField.setAccessible(true);
                Description annotation = declaredField.getAnnotation(Description.class);
                logger.info("@Description " + annotation.name() + " " + annotation.partNumber() + " " + annotation.price());
                logger.info(declaredField.getType() + " " + declaredField.getName());
            }
        }
    }
}
